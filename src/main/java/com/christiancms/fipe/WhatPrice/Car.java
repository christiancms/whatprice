package com.christiancms.fipe.WhatPrice;

public class Car {
	
	private String value;
	private String brand;
	private String model;
	private int modelYear;
	private String fuel;
	private String fipeCode;
	private String referenceMonth;
	private int vehicleType;
	private String fuelFirstLetter;
	
	
	
	public Car(String value, String brand, String model, int modelYear, String fuel, String fipeCode,
			String referenceMonth, int vehicleType, String fuelFirstLetter) {
		super();
		this.value = value;
		this.brand = brand;
		this.model = model;
		this.modelYear = modelYear;
		this.fuel = fuel;
		this.fipeCode = fipeCode;
		this.referenceMonth = referenceMonth;
		this.vehicleType = vehicleType;
		this.fuelFirstLetter = fuelFirstLetter;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getModelYear() {
		return modelYear;
	}
	public void setModelYear(int modelYear) {
		this.modelYear = modelYear;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public String getFipeCode() {
		return fipeCode;
	}
	public void setFipeCode(String fipeCode) {
		this.fipeCode = fipeCode;
	}
	public String getReferenceMonth() {
		return referenceMonth;
	}
	public void setReferenceMonth(String referenceMonth) {
		this.referenceMonth = referenceMonth;
	}
	public int getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getFuelFirstLetter() {
		return fuelFirstLetter;
	}
	public void setFuelFirstLetter(String fuelFirstLetter) {
		this.fuelFirstLetter = fuelFirstLetter;
	}
	
	

}
