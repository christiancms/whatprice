package com.christiancms.fipe.WhatPrice;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class APIController {

	@RequestMapping(method = { RequestMethod.GET }, value = { "/carros/marcas" })
	public void getBrand(HttpServletResponse response) {
		if (response.equals(HttpServletResponse.SC_OK)) {
			getModel(response, "23");
		}
	}

	@RequestMapping(method = { RequestMethod.GET }, value = { "/{code}/modelos" })
	public void getModel(HttpServletResponse response, @PathVariable("code") String code) {
		if (response.equals(HttpServletResponse.SC_OK)) {
			getYear(response, "5637");
		}
	}

	@RequestMapping(method = { RequestMethod.GET }, value = { "/{code}/anos" })
	public void getYear(HttpServletResponse response, @PathVariable("code") String code) {
		if (response.equals(HttpServletResponse.SC_OK)) {
			getCarByFuel(response, "2016-1");
		}
	}

	@RequestMapping(method = { RequestMethod.GET }, value = { "/{code}" })
	public void getCarByFuel(HttpServletResponse response, @PathVariable("code") String code) {
		if (response.equals(HttpServletResponse.SC_OK)) {

		}
	}
}
