package com.christiancms.fipe.WhatPrice;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/com/christiancms/fipe/WhatPrice/tests", glue = {
		"com.christiancms.fipe.WhatPrice.steps" }, plugin = "json:target/cucumber-report.json")
public class AppTest {

}
