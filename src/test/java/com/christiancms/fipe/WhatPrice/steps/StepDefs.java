package com.christiancms.fipe.WhatPrice.steps;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Assert;

import com.christiancms.fipe.WhatPrice.Car;
import com.jayway.restassured.response.Response;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {

	Response response;
	private String url = "https://parallelum.com.br/fipe/api/v1/carros/marcas";
	Car car = new Car("R$ 59.907,00", "GM - Chevrolet", "CRUZE LTZ 1.8 16V FlexPower 4p Aut.", 2016, "Gasolina",
			"004381-8", "março de 2020 ", 1, "G");

	@Given("^I know car brand \"([^\"]*)\"$")
	public void i_know_car_brand(String url) throws Throwable {
		this.url = url;
		response = given().when().get(url);
		Assert.assertEquals(200, response.getStatusCode());
	}

	@And("^the model \"([^\"]*)\"$")
	public void the_model(String urlModel) {
		this.url = url.concat(urlModel);
		response = given().when().get(url);
		Assert.assertEquals(200, response.getStatusCode());
	}

	@And("^vehicle year \"([^\"]*)\"$")
	public void vehicle_year(String urlYear) {
		this.url = url.concat(urlYear);
		response = given().when().get(url);
		Assert.assertEquals(200, response.getStatusCode());
	}

	@When("^I make a query \"([^\"]*)\"$")
	public void i_make_a_query(String urlCode) {
		this.url = url.concat(urlCode);
		response = given().when().get(url);
		Assert.assertEquals(200, response.getStatusCode());
	}

	@Then("I should receive updated price")
	public void i_should_receive_updated_price() {
		Assert.assertEquals(car.getValue(), response.jsonPath().getString("Valor"));
		Assert.assertEquals(car.getBrand(), response.jsonPath().getString("Marca"));
		Assert.assertEquals(car.getModel(), response.jsonPath().getString("Modelo"));
		Assert.assertEquals(car.getModelYear(), response.jsonPath().getInt("AnoModelo"));
		Assert.assertEquals(car.getFuel(), response.jsonPath().getString("Combustivel"));
		Assert.assertEquals(car.getFipeCode(), response.jsonPath().getString("CodigoFipe"));
		Assert.assertEquals(car.getReferenceMonth(), response.jsonPath().getString("MesReferencia"));
		Assert.assertEquals(car.getVehicleType(), response.jsonPath().getInt("TipoVeiculo"));
		Assert.assertEquals(car.getFuelFirstLetter(), response.jsonPath().getString("SiglaCombustivel"));
	}

}
