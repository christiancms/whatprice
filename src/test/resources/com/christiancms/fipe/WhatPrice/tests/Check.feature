@tag
Feature: Test flow until fipe API

  @tag1
  Scenario: Request the price of some vehicle
    Given I know car brand "https://parallelum.com.br/fipe/api/v1/carros/marcas"
    And the model "/23/modelos"
    And vehicle year "/5637/anos"
    When I make a query "/2016-1"
    Then I should receive updated price