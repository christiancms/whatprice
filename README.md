
# WhatPrice

##Challenge to show skills with BDD Gherkin 

Description:

Application to test fipe API 
Explainig FIPE API:
The Fipe Table expresses average vehicle prices in the national market, serving only as a parameter for negotiations or evaluations. The prices actually charged vary depending on the region, conservation, color, accessories or any other factor that may influence the conditions of supply and demand for a specific vehicle.


This especific test is expect to find Car information at 
[https://parallelum.com.br/fipe/api/v1](https://parallelum.com.br/fipe/api/v1)
and to restrict result for test this flow, so the criteria was determinated by challenge like this:
  
  * "Valor": "R$ 59.907,00",
  * "Marca": "GM - Chevrolet",
  * "Modelo": "CRUZE LTZ 1.8 16V FlexPower 4p Aut.",
  * "AnoModelo": 2016,
  * "Combustivel": "Gasolina",
  * "CodigoFipe": "004381-8",
  * "MesReferencia": "março de 2020 ",
  * "TipoVeiculo": 1,
  * "SiglaCombustivel": "G"
  
**Automated Test Environment**

The Feature contain one Scenario

* Scenario: Request the price of some vehicle
   * **Given** I know car brand "https://parallelum.com.br/fipe/api/v1/carros/marcas"
   * **And** the model "/23/modelos"
   * **And** vehicle year "/5637/anos"
   * **When** I make a query "/2016-1"
   * **Then** I should receive updated price
---
1. Checkout project from repo
2. To run, you will need to install maven, then:

>mvn clean install
